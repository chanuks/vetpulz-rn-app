import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Auth } from 'aws-amplify';
import { SafeAreaView } from 'react-native-safe-area-context';
import AppTextInput from '../components/AppTextInput';
import AppButton from '../components/AppButton';
import axios from 'axios';
import { MaterialIcons } from '@expo/vector-icons';
import DatePicker from 'react-native-datepicker'
import { set } from 'react-native-reanimated';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { openDatabase } from 'react-native-sqlite-storage';


export default function AddPet({ navigation }) {


    // var db = openDatabase({ name: 'UserDatabase.db' });

    var moment = require('moment');
    const [title, setTitle] = useState('');
    const [name, setName] = useState('');
    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [insertID, setInsertID] = useState('');
    const [clientID, setClientID] = useState('');
    const [veterinary, setVeterinary] = useState('');
    const [awsUserId, setAwsUserId] = useState('');
    var date = moment()
        .utcOffset('+05:30')
        .format('YYYY-MM-DD hh:mm:ss');


 


    // useEffect(() => {
    //     db.transaction(function (txn) {
    //         txn.executeSql(
    //             "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
    //             [],
    //             function (tx, res) {
    //                 console.log('item:', res.rows.length);
    //                 if (res.rows.length == 0) {
    //                     txn.executeSql('DROP TABLE IF EXISTS table_user', []);
    //                     txn.executeSql(
    //                         'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, client_id VARCHAR(30), client_insert_id VARCHAR(30), aws_user_id VARCHAR(60), veterinary_hospital VARCHAR(255))',
    //                         []
    //                     );
    //                 }
    //             }
    //         );
    //     });
    // }, []);





    // const saveOwner = () => {
    //     axios.post(`https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/petowner?tenant=${global.MyVet}`, {
    //         version: '1',
    //         created_at: date,
    //         created_by: 0,
    //         updated_at: date,
    //         updated_by: 0,
    //         active: true,
    //         address: address,
    //         email: email,
    //         mobileNumber: mobile,
    //         name: name,
    //         title: title,
    //         clientID: null,
    //         notification: 0,


    //     })
    //         .then(function (response) {
    //             // handle success
    //             // alert(JSON.stringify(response.data.insertId));

    //             setInsertID(JSON.stringify(response.data.insertId));

    //             setClientID(generateClientId(JSON.stringify(response.data.insertId)));

    //             // alert('Client ID'+global.client+'   Insert ID'+global.insert);

    //             updateOwner();


    //             console.log(response);
    //             navigation.navigate('AddPet');
    //         })
    //         .catch(function (error) {
    //             // handle error
    //             alert(error.message);
    //         });
    // }


    const storeClientInsertID = async (value) => {
        try {
          await AsyncStorage.setItem('ClientInsertID', value)
        } catch (e) {
          console.log(e.message);
        }
      }

      const storeClientID = async (value) => {
        try {
          await AsyncStorage.setItem('ClientID', value)
        } catch (e) {
           console.log(e.message);
        }
      }



    
    const saveOwner = () => {
        axios.post('https://i2ehtrtri6.execute-api.us-east-1.amazonaws.com/TESTING/Client', {
          
            "client" : {
                "title" : title,
                "name": name,
                "mobileNumber" : mobile,
                "email" : email,
                "address" : address,
                "notification" : false,
                "loggeduser" : 0
              }


        }, { headers: {
            'Authorization': 'Basic aGlzOmhpczEyMzQ1',
            'X-tenantID':global.MyVet
        }})
            .then(function (response) {
             
                console.log("client id"+response.data.clientID);
    

                global.ClientID = response.data.id;
                alert("Pet Owner Registered Successfully");
                navigation.navigate('AddPet',{Client:response.data});
            })
            .catch(function (error) {
      
                alert(error.message);
            });
    }
    // const updateOwner = () => {
    //     axios.patch(`https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/petowner?tenant=${global.MyVet}&insert=${global.insert}&client=${global.client}`, {



    //     })
    //         .then(function (response) {
    //             // handle success



    //             console.log(response);



    //             alert("Pet Owner Registered Successfully");
    //             //navigation.navigate('AddPet');
    //         })
    //         .catch(function (error) {
    //             // handle error
    //             alert(error.message);
    //         });
    // }




    async function signUp() {

        try {

            await Auth.signUp({ username, password, attributes: { email } });

            console.log(' Sign-up Confirmed');

            navigation.navigate('ConfirmSignUp');

        } catch (error) {

            console.log(' Error signing up...', error);

        }

    }


    return (

        

        <SafeAreaView style={styles.safeAreaContainer}>

            <KeyboardAvoidingView
                style={styles.container}
                behavior={Platform.OS == "ios" ? "padding" : "height"}
            >

                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.container} behavior="padding">

                        <Text style={styles.title}>Enter Pet's Owner Details</Text>


                        <Image
                            source={require('../../Image/7312.jpg')}
                            style={{
                                width: 300,
                                height: 100,
                                resizeMode: 'cover',
                                margin: 30
                            }}
                        />

                        <AppTextInput

                            value={title}

                            onChangeText={text => setTitle(text)}



                            placeholder="Enter Title  Mr. / Mrs. / Dr. etc"

                            autoCapitalize="none"

                            keyboardType="default"

                            textContentType="none"

                        />

                        <AppTextInput

                            value={name}

                            onChangeText={text => setName(text)}



                            placeholder="Enter Name"

                            autoCapitalize="none"

                            keyboardType="default"

                            textContentType="none"

                        />



                        <AppTextInput

                            value={address}

                            onChangeText={text => setAddress(text)}


                            placeholder="Enter Address"

                            autoCapitalize="none"

                            keyboardType="default"

                            textContentType="none"

                        />


                        <AppTextInput

                            value={email}

                            onChangeText={text => setEmail(text)}



                            placeholder="Enter Email.."

                            autoCapitalize="none"

                            keyboardType="default"

                            textContentType="none"

                        />

                        <AppTextInput

                            value={mobile}

                            onChangeText={text => setMobile(text)}



                            placeholder="Enter Mobile Number.."

                            autoCapitalize="none"

                            keyboardType="default"

                            textContentType="none"

                        />








                        <View style={styles.bottom}>
                            <AppButton title="Next" onPress={() => saveOwner()} />

                        </View>

                        <View style={styles.footerButtonContainer}>



                        </View>

                    </View>

                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>

        </SafeAreaView>

    );

}


const styles = StyleSheet.create({

    safeAreaContainer: {

        flex: 1,

        backgroundColor: 'white'

    },

    container: {

        flex: 1,

        alignItems: 'center'

    },

    title: {

        fontSize: 24,

        color: '#202020',

        fontWeight: '500',

        marginVertical: 15

    },

    footerButtonContainer: {

        marginVertical: 15,

        justifyContent: 'center',

        alignItems: 'center'

    },

    forgotPasswordButtonText: {

        color: 'darkred',

        fontSize: 16,

        fontWeight: '400'

    }

});