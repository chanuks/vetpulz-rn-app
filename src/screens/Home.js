import React, { useEffect, useState } from 'react';

import { View, Text, StyleSheet, Button, Image, TouchableOpacity } from 'react-native';

import { Auth } from 'aws-amplify';

import axios from 'axios';

import Dropdown from '../components/Dropdown';

import { MaterialCommunityIcons } from '@expo/vector-icons';

import { FontAwesome } from '@expo/vector-icons';

import AsyncStorage from '@react-native-async-storage/async-storage';






export default function Home({ navigation, updateAuthState }) {


    const [clientId, setClientId] = useState('');
    const [clientInsertId, setClientInsertId] = useState('');
    const [veterinary, setVeterinary] = useState('');
    const [awsUser, setAwsUser] = useState('');





    // useEffect(() => {
    //     getVeterinaryHospitals()
    // }, []);


    useEffect(() => {

        getClient();
    });

    useEffect(() => {

        getClientInsert();
    });

    useEffect(() => {

        getHospital();
    });
    useEffect(() => {

        getAWSUser();
    });

    useEffect(() => {

       handleLocal();
    });




    const handleRegister = () => {



        let route = '';

        if (clientId == null) {

            route = 'SelectVeterinary'

        } else if (clientId !== null) {

            global.MyVet = veterinary;
            global.client = clientId;
            global.insert = clientInsertId;
            route = 'AddPet'
        }

        return route;

    };



    Auth.currentUserInfo({
        bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
    }).then(user =>
        AsyncStorage.setItem('AWS_User', user.id))
        // console.log("AWS user " + JSON.stringify(user.id)))
        // AsyncStorage.setItem('AWS_User', JSON.stringify(user.id)))
        .catch(err => console.log(err));



    const handleLocal = () => {

        global.MyVet = veterinary;
        global.client = clientId;
        global.insert = clientInsertId;
        
    };







    const getClient = () => {
        // Function to get the value from AsyncStorage
        AsyncStorage.getItem('Client').then(
            (value) =>
                // AsyncStorage returns a promise
                // Adding a callback to get the value
                setClientId(value),
            // Setting the value in Text
        );


    };

    const getClientInsert = () => {
        // Function to get the value from AsyncStorage
        AsyncStorage.getItem('Client_InsertID').then(
            (value) =>
                // AsyncStorage returns a promise
                // Adding a callback to get the value
                setClientInsertId(value),
            // Setting the value in Text
        );


    };

    const getHospital = () => {
        // Function to get the value from AsyncStorage
        AsyncStorage.getItem('VeterinaryHospital').then(
            (value) =>
                // AsyncStorage returns a promise
                // Adding a callback to get the value
                setVeterinary(value),
            // Setting the value in Text
        );


    };

    const getAWSUser = () => {
        // Function to get the value from AsyncStorage
        AsyncStorage.getItem('AWS_User').then(
            (value) =>
                // AsyncStorage returns a promise
                // Adding a callback to get the value
                setAwsUser(value),
            // Setting the value in Text
        );


    };



    // const getVeterinaryHospitals = () => {
    //     axios
    //         .get('https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/veterinary')
    //         .then(function (response) {





    //             //  alert("AWS User Information "+JSON.stringify(Auth.currentUserPoolUser()));
    //         })
    //         .catch(function (error) {

    //             alert(error.message);
    //         })
    //         .finally(function () {

    //             alert('Finally called');
    //         });
    // };










    async function signOut() {

        try {


            await Auth.signOut();

            updateAuthState('loggedOut');



        } catch (error) {

            console.log('Error signing out: ', error);

        }

    }


    return (

        <View style={styles.container}>

            <Text style={styles.title}> Vetpulz Client Dashboard</Text>
            <Image
                source={require('../../Image/veterinarian.jpg')}
                style={{
                    width: 450,
                    height: 300,
                    resizeMode: 'cover',
                    margin: 30
                }}
            />


            {/* <TouchableOpacity onPress={() => navigation.navigate('AddPet')}>

                <Text style={styles.title}>

                    Register Pet

                </Text>

            </TouchableOpacity> */}


            <View style={{ flex: 3, flexDirection: 'coloumn', alignItems: 'stretch' }}>
                <View style={styles.button}>

                    <TouchableOpacity onPress={() => navigation.navigate(handleRegister())}>
                    {/* <TouchableOpacity onPress={() => navigation.navigate('SelectVeterinary')}> */}
                        <View style={{ flexDirection: 'row', justifyContent: 'center', width: 300 }}>
                            <MaterialCommunityIcons

                                name='dog'

                                size={30}

                                color="white"

                                style={styles.icon}

                            />


                            <Text style={styles.buttonText}>

                                Register Pet

                            </Text>

                        </View>


                    </TouchableOpacity>
                </View>

                <View style={styles.button}>

                    <TouchableOpacity onPress={() => navigation.navigate('ViewPets')}>

                        <View style={{ flexDirection: 'row', justifyContent: 'center', width: 300 }}>
                            <FontAwesome

                                name='search'

                                size={22}

                                color="white"

                                style={styles.icon}

                            />


                            <Text style={styles.buttonText}>

                                View your Pets

                            </Text>

                        </View>

                    </TouchableOpacity>
                </View>
            </View>



            {/* <Dropdown /> */}




            {/* <Text>{global.MyVet}</Text>
            <Text>{global.client}</Text>
            <Text>{global.insert}</Text>
            <Text>{'Client from Async' + clientId}</Text>
            <Text>{'Client insert' + clientInsertId}</Text>
            <Text>{'hospital' + veterinary}</Text>
            <Text>{'Aws User  ' + awsUser}</Text> */}






            <Button title="Sign Out" color="blue" onPress={signOut} />

        </View>

    );

}


const styles = StyleSheet.create({

    container: {

        flex: 1,

        alignItems: 'center',

        marginTop: 20

    },
    title: {

        fontSize: 26,

        color: '#202020',

        fontWeight: '500',

        marginVertical: 15

    },

    icon: {

        marginRight: 10

    },

    button: {


        marginVertical: 10,

        borderRadius: 25,

        justifyContent: 'center',

        alignItems: 'center',

        padding: 20,

        width: '80%',

        backgroundColor: 'purple'

    },

    buttonText: {

        color: 'white',

        fontSize: 24,

        fontWeight: '600',



    }


});