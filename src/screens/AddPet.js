import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, Alert, Modal, TouchableHighlight, Button, Platform ,  KeyboardAvoidingView} from 'react-native';
import { Auth } from 'aws-amplify';
import { SafeAreaView } from 'react-native-safe-area-context';
import AppTextInput from '../components/AppTextInput';
import AppButton from '../components/AppButton';
import { set } from 'react-native-reanimated';
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Feather';
import DateTimePicker from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default function AddPet({ navigation, route }) {

    var moment = require('moment');
    const [modalVisible, setModalVisible] = useState(false);
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [gender, setGender] = useState('');
    const [attitude, setAttitude] = useState('');
    const [species, setSpecies] = useState([]);
    const [valueS, setValueS] = useState('Dog');
    const [breed, setBreed] = useState([]);
    const [valueB, setValueB] = useState('');
    const [petID, setPetID] = useState('')

    const [date, setDate] = useState(new Date(1598051730000));
    var currentDate = moment()
        .utcOffset('+05:30')
        .format('YYYY-MM-DD hh:mm:ss');
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const Client = route.params.Client;


    let owner = global.client;

    useEffect(() => {
        getSpecies()
    }, []);

    useEffect(() => {
        console.log("Client Inforamtion "+ Client.clientID)
    });


 

    const getSpecies = () => {
        axios
            .get(`https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/animalspecies?tenant=${global.MyVet}`)
            .then(function (response) {



                setSpecies(response.data);

                console.log(species);

                // getBreeds();

            })
            .catch(function (error) {

                console.log(error.message);
            })
            .finally(function () {


            });
    };



    const getBreeds = (itemValue) => {
        axios
            .get('https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/animalbreed?tenant=D0001&species=' + itemValue)
            .then(function (response) {



                setBreed(response.data);

                console.log(breed);


            })
            .catch(function (error) {

                console.log(error.message);
            })
            .finally(function () {


            });
    };


    const getBreedOnChange = (itemValue) => {
        setValueS(itemValue);
        getBreeds(itemValue);
    }

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);

    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    // const savePet = () => {
    //     axios.post(`https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/pet?tenant=${global.MyVet}`, {
    //         version: '1',
    //         created_at: currentDate,
    //         created_by: 0,
    //         updated_at: currentDate,
    //         updated_by: 0,
    //         active: true,
    //         dob: date,
    //         gender: gender,
    //         name: name,
    //         petIdentificationNo: null,
    //         breed_id: valueB,
    //         owner_id: global.insert,
    //         photoUrl: "-",
    //         attitude: "-",
    //         previousNumber: "-"


    //     })
    //         .then(function (response) {
    //             // handle success
    //             // alert("Insert ID for Pet " + JSON.stringify(response.data.insertId));

    //             setPetID(generatePetId(JSON.stringify(response.data.insertId)));
    //             // setInsertID(JSON.stringify(response.data.insertId));

    //             // setClientID( generateClientId(JSON.stringify(response.data.insertId)));

    //             // alert('Pet ID  '+global.pet);

    //             updatePet();


    //             console.log(response);
    //             // navigation.navigate('AddPet');
    //         })
    //         .catch(function (error) {
    //             // handle error
    //             console.log(error.message);
    //         });
    // }



    const savePet = () => {
        axios.post( 'https://i2ehtrtri6.execute-api.us-east-1.amazonaws.com/TESTING/Pet', {
            
        
              "pet" : {
                "name" : name,
                "dob" : date,
                "breed" :valueB,
                "owner" : Client.id,
                "gender" : gender,
                "photoUrl" : "",
                "previousNumber" : "",
                "attitude" : "",
                "loggeduser" : 0,
                "petIdentificationNo" : 0
    
                
              }
        
        
            }, { headers: {
                'Authorization': 'Basic aGlzOmhpczEyMzQ1',
                'X-tenantID':global.MyVet
            }
        
        
    


        })
            .then(function (response) {
                // handle success
                // alert("Insert ID for Pet " + JSON.stringify(response.data.insertId));

                // setPetID(generatePetId(JSON.stringify(response.data.insertId)));
                // setInsertID(JSON.stringify(response.data.insertId));

                // setClientID( generateClientId(JSON.stringify(response.data.insertId)));

                // alert('Pet ID  '+global.pet);

                // updatePet();


                console.log(response.data);

                alert("Pet Registered Successfully");
                navigation.navigate('ViewPets',{ClientId:Client.id});
           
            })
            .catch(function (error) {
                // handle error
                console.log(error.message);
            });
    }


    // const generatePetId = (insertId) => {

    //     let generatedPetID;

    //     global.petInsert = insertId;


    //     if (insertId.length == 2) {
    //         generatedPetID = global.client + '/' + 'P000' + insertId;
    //     } else if (insertId.length == 3) {
    //         generatedPetID = global.client + '/' + 'P00' + insertId;
    //     } else if (insertId.length == 4) {
    //         generatedPetID = global.client + '/' + 'P0' + insertId;
    //     } else if (insertId.length == 5) {
    //         generatedPetID = global.client + '/' + 'P' + insertId;
    //     } else {
    //         alert('Inavalid Pet ID');
    //     }

    //     global.pet = generatedPetID;


    //     return generatedPetID;

    // }

    // const updatePet = () => {
    //     axios.patch(`https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/pet?tenant=${global.MyVet}&insert=${global.petInsert}&pet=${global.pet}`, {



    //     })
    //         .then(function (response) {
    //             // handle success


    //             // alert('Pet Inserted '+JSON.stringify(response.data.insertId));

    //             console.log(response.data);

    //             alert("Pet Registered Successfully");
    //             navigation.navigate('ViewPets');
    //         })
    //         .catch(function (error) {
    //             // handle error
    //             (error.message);
    //         });
    // }






    return (


        <SafeAreaView style={styles.safeAreaContainer}>
            <View style={styles.container}>
                <Text style={styles.title}>Register Pet </Text>



                {/* <Image
                    source={require('../../Image/6677.jpg')}
                    style={{
                        width: '80%',
                        height: 100,
                        resizeMode: 'contain',
                        margin: 30
                    }}
                /> */}
                <View style={styles.centeredView}>




                    <AppTextInput

                        value={name}

                        onChangeText={text => setName(text)}

                        leftIcon="cat"

                        placeholder="Enter pet's name"

                        autoCapitalize="none"

                        keyboardType="default"

                        textContentType="none"

                    />


                    <AppTextInput

                        value={gender}

                        onChangeText={text => setGender(text)}

                        leftIcon="gender-male-female"

                        placeholder="Enter pet's gender"

                        autoCapitalize="none"

                        keyboardType="default"

                        textContentType="none"

                    />



                    {/* <DatePicker
                    style={{ width: 350 }}
                    date={birthdate}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="1990-05-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        placeholderText: 'Enter Date',
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => { setBirthdate(date) }}
                  /> */}




                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => {
                            Alert.alert("Modal has been closed.");
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>
                                <Text style={styles.modalText}>Select your pet's breed & species</Text>

                                <View style={{ flex: 6, flexDirection: 'coloumn', alignItems: 'stretch' }}>
                                    <View style={{ flex: 3 }}>



                                        <Picker
                                            style={{ height: 10, width: 300, marginBottom: 100 }}
                                            mode="dropdown"
                                            selectedValue={valueS}
                                            onValueChange={(itemValue, itemIndex) => getBreedOnChange(itemValue)}
                                        >
                                            {species.map((item, index) =>
                                                <Picker.Item
                                                    key={`${item[index]}`}
                                                    label={item.name}
                                                    value={item.id} />
                                            )}
                                        </Picker>
                                    </View>

                                    <View style={{ flex: 3 }}>

                                        <Picker
                                            style={{ height: 10, width: 300, marginBottom: 100 }}
                                            mode="dropdown"
                                            selectedValue={valueB}
                                            onValueChange={(itemValue, itemIndex) => setValueB(itemValue)} >
                                            {breed.map((item, index) =>
                                                <Picker.Item
                                                    key={`${item[index]}`}
                                                    label={item.name}
                                                    value={item.id} />
                                            )}
                                        </Picker>

                                    </View>
                                </View>
                                <TouchableHighlight
                                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                                    onPress={() => {
                                        setModalVisible(!modalVisible);
                                    }}
                                >
                                    <Text style={styles.textStyle}>Close</Text>
                                </TouchableHighlight>
                            </View>
                        </View>
                    </Modal>

                    <TouchableHighlight
                        style={styles.openButton}
                        onPress={() => {
                            setModalVisible(true);
                        }}
                    >
                        <Text style={styles.textStyle}>Select your pet's Specie & Breed</Text>
                    </TouchableHighlight>

                    <View>
                        <TouchableHighlight style={styles.openButton} onPress={showDatepicker} >
                            <Text style={styles.textStyle}>Select pet's date of birth</Text>
                        </TouchableHighlight>
                    </View>

                    <View style={{ width: 300 }}>
                        {/* <View>
                          
                            <Button  onPress={showDatepicker} title="Show date picker!" />
                        </View> */}



                        {show && (
                            <DateTimePicker
                                testID="dateTimePicker"
                                value={date}
                                mode={mode}
                                is24Hour={true}
                                display="default"
                                onChange={onChange}
                            />
                        )}
                    </View>






                </View>

    


                <AppButton title="Save" onPress={() => savePet()} />

                <View style={styles.footerButtonContainer}>



                </View>

            </View>



        </SafeAreaView>

        

    );

}


const styles = StyleSheet.create({

    safeAreaContainer: {

        flex: 1,

        backgroundColor: 'white'

    },

    container: {

        flex: 1,

        alignItems: 'center'

    },

    title: {

        fontSize: 24,

        color: '#202020',

        fontWeight: '500',

        marginVertical: 15

    },

    footerButtonContainer: {

        marginVertical: 15,

        justifyContent: 'center',

        alignItems: 'center'

    },

    forgotPasswordButtonText: {

        color: 'darkred',

        fontSize: 16,

        fontWeight: '400'

    },
    centeredView: {
        flex: 1,

        alignItems: "center",
        marginTop: 5
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "deepskyblue",
        borderRadius: 20,
        padding: 10,
        marginVertical: 10,
        height: 40,
        width: 370

    },
    textStyle: {
        color: "white",
        textAlign: 'auto',
        fontSize: 18,
        marginLeft: 15
    },
    modalText: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        textAlign: "center"
    }
});