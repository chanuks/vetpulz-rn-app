import React, { useState, useEffect } from 'react';

import { View, Text, StyleSheet, Image, Button, TouchableOpacity } from 'react-native';

import { Auth } from 'aws-amplify';

import axios from 'axios';

import Dropdown from '../components/Dropdown';

import { Picker } from '@react-native-picker/picker';

import { Tile } from 'react-native-elements';
import AppButton from '../components/AppButton';





export default function Home({ navigation, updateAuthState }) {



    const [veterinary, setVeterinary] = useState([]);
    const [valueV, setValueV] = useState('D0001');

    global.MyVet = valueV;

    useEffect(() => {
        getVeterinaryHospitals()
    }, []);

    //Get veterinary hospitals with tenant
    const getVeterinaryHospitals = () => {
        axios
            .get('https://i2ehtrtri6.execute-api.us-east-1.amazonaws.com/TESTING/HrHospitalDetails',
                {
                    headers: {
                        'Authorization': 'Basic aGlzOmhpczEyMzQ1',
                        'X-tenantID':'D0001'
                    }
                })
            .then(function (response) {

                // alert(JSON.stringify(response.data));

                setVeterinary(response.data);

                console.log("helooo"+response.data);

                // console.log(veterinary);


            })
            .catch(function (error) {

                alert(error.message);
            })
            .finally(function () {


            });
    };









    async function signOut() {

        try {

            await Auth.signOut();

            updateAuthState('loggedOut');

        } catch (error) {

            console.log('Error signing out: ', error);

        }

    }






    return (

        <View style={styles.container}>

            <Text style={styles.title}> Please select the veterinary hospital.</Text>



            <Image
                source={require('../../Image/7620.jpg')}
                style={{
                    width: 450,
                    height: 300,

                }}
            />




            <Picker
                style={{ height: 20, width: 300, marginBottom: 100 }}
                mode="dropdown"
                selectedValue={valueV}
                onValueChange={(itemValue, itemIndex) => setValueV(itemValue)} >
                {veterinary.map((item, index) =>
                    <Picker.Item
                        key={`${item[index]}`}
                        label={item.hospitalName}
                        value={item.hospitalHINId} />
                )}
            </Picker>

            {/* <TouchableOpacity
                style={styles.button}
                onPress={getVeterinaryHospitals}>
                <Text>Get Veterinary Hospitals</Text>
            </TouchableOpacity> */}





            <View style={styles.bottom}>
                <AppButton title="Next" onPress={() => navigation.navigate('AddOwner')} />

            </View>





        </View>

    );

}


const styles = StyleSheet.create({

    container: {

        flex: 1,

        flexDirection: 'column',

        justifyContent: 'flex-start',

        alignItems: 'center',

        marginTop: 20

    },
    title: {

        fontSize: 26,

        color: '#202020',

        fontWeight: '500',

        marginVertical: 15

    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
    }

});