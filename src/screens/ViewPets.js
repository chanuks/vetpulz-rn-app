

import React, { useState, useEffect } from 'react';

import { View, Text, StyleSheet, Button, Image, TouchableOpacity, FlatList } from 'react-native';

import { Auth } from 'aws-amplify';

import axios from 'axios';

import Dropdown from '../components/Dropdown';

import { MaterialCommunityIcons } from '@expo/vector-icons';

import { FontAwesome } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';





export default function ViewPets({ navigation, updateAuthState, route }) {


    const [pets, setPets] = useState([]);
    const [vetID, setVetID] = useState('');
    const [ownerID, setOwnerID] = useState('');


 


    
    useEffect(()=>{
        getOwnerID();
    });

    useEffect(()=>{
        getVeterinaryID();
    });

    useEffect(() => {
        getOwnerPets()
    }, [vetID,ownerID]);



    const getVeterinaryID = async () => {

       await AsyncStorage.getItem('VeterinaryHospital').then(
            (value) =>

                setVetID(value),

        );
    };

    const getOwnerID = async () => {

       await AsyncStorage.getItem('Client_InsertID').then(
            (value) =>

                setOwnerID(value),

        );
    };

    // const getOwnerPets = () => {
    //     axios
    //         .get('https://xilwknb747.execute-api.us-east-1.amazonaws.com/PROD/pet/owner?tenant='+vetID+'&owner='+ownerID)
    //         .then(function (response) {



    //             setPets(response.data);


               



    //         })
    //         .catch(function (error) {

    //             console.log(error.message);
    //         })
    //         .finally(function () {


    //         });
    // };



    
    const getOwnerPets = () => {
        axios
            .get('https://i2ehtrtri6.execute-api.us-east-1.amazonaws.com/TESTING/Pet/Client/'+global.ClientID,
             
            {
                headers: {
                'Authorization': 'Basic aGlzOmhpczEyMzQ1',
                'X-tenantID':'D0001'  }
        
        })
            .then(function (response) {



                console.log(response.data);
                setPets(response.data);


               



            })
            .catch(function (error) {

                console.log(error.message);
            })
            .finally(function () {


            });
    };



    async function signOut() {

        try {

         
            AsyncStorage.clear();
            
            await Auth.signOut();

            updateAuthState('loggedOut');


           

        } catch (error) {

            console.log('Error signing out: ', error);

        }

    }


    return (

        <View style={styles.container}>

            <Text style={styles.title}> Your Pets</Text>
            {/* <Image
                source={require('../../Image/veterinarian.jpg')}
                style={{
                    width: 450,
                    height: 300,
                    resizeMode: 'cover',
                    margin: 30
                }}
            /> */}


            <FlatList
                data={pets}
                keyExtractor={result => result.id.toString()}
                renderItem={({ item }) => {


                    return (
                        <View style={styles.containerC}>

                            <Image style={styles.image} source={require('../../Image/doggy3.png')}></Image>
                            <Text style={styles.name}>{item.name}</Text>
                            <Text style={styles.nameT} >Gender - {item.gender} , Pet Id - {item.petIdentificationNo} </Text>
                            <Text></Text>
                        </View>);

                }}

            />




{/* 
<Text>{vetID}+{ownerID}</Text> */}



            <Button title="Home" color="blue" onPress={() => navigation.navigate('Home')} />

            <Button title="Sign Out" color="blue" onPress={signOut} />

        </View>

    );

}


const styles = StyleSheet.create({

    container: {

        flex: 1,

        alignItems: 'center',

        marginTop: 20,



    },
    title: {

        fontSize: 32,

        color: '#202020',

        fontWeight: "bold",

        marginVertical: 15

    },

    icon: {

        marginRight: 10

    },

    button: {


        marginVertical: 10,

        borderRadius: 25,

        justifyContent: 'center',

        alignItems: 'center',

        padding: 20,

        width: '80%',

        backgroundColor: 'purple'

    },

    buttonText: {

        color: 'white',

        fontSize: 24,

        fontWeight: '600',



    },
    containerC: { marginLeft: 15, backgroundColor: "lavender", borderRadius: 18, marginTop: 10 },
    image: {
        width: 80,
        height: 80,
        borderRadius: 4,
        marginBottom: 5,
        marginLeft: 130
    },
    name: {
        fontWeight: 'bold',
        fontSize: 28
    },
    nameT: {

        fontSize: 20
    }


});