import React, { useState, useEffect } from 'react';
import { ActivityIndicator, View,Image } from 'react-native';
import Amplify, { Auth } from 'aws-amplify';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import config from './aws-exports';
import SignIn from './src/screens/SignIn';
import SignUp from './src/screens/SignUp';
import ConfirmSignUp from './src/screens/ConfirmSignUp';
import Home from './src/screens/Home';
import AddPet from './src/screens/AddPet';
import SelectVeterinary from './src/screens/SelectVeterinary';
import AddOwner from './src/screens/AddOwner';
import ViewPets from './src/screens/ViewPets';

Amplify.configure(config);

function App() {


  const [isUserLoggedIn, setUserLoggedIn] = useState('initializing');

  const AuthenticationStack = createStackNavigator();

  const AppStack = createStackNavigator();


  const AuthenticationNavigator = props => {

    return (

      <AuthenticationStack.Navigator headerMode="none">

        <AuthenticationStack.Screen name="SignIn">

          {screenProps => (

            <SignIn {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AuthenticationStack.Screen>

        <AuthenticationStack.Screen name="SignUp" component={SignUp} />

        <AuthenticationStack.Screen

          name="ConfirmSignUp"

          component={ConfirmSignUp}

        />

      </AuthenticationStack.Navigator>

    );

  };

  function LogoTitle() {
    return (
      <Image
        style={{ width: 100, height: 45 }}
        source={require('./Image/Vetpulz-Logo.png')}
      />
    );
  }


  const AppNavigator = props => {

    return (

      <AppStack.Navigator>

        <AppStack.Screen name="Home" options={{headerTitle: props => <LogoTitle {...props} />}}>

          {screenProps => (

            <Home {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AppStack.Screen>

        <AppStack.Screen name="AddPet"  options={{title:'Pet Details'},{headerTitle: props => <LogoTitle {...props} />}}>
          {screenProps => (

            <AddPet {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AppStack.Screen>

        <AppStack.Screen name="SelectVeterinary"  options={{title:'Select Veterinary Hospital'},{headerTitle: props => <LogoTitle {...props} />}}>
          {screenProps => (

            <SelectVeterinary {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AppStack.Screen>

        <AppStack.Screen name="AddOwner"   options={{title:'Pet Owner Details'},{headerTitle: props => <LogoTitle {...props} />}}>
          {screenProps => (

            <AddOwner {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AppStack.Screen>
        
        <AppStack.Screen name="ViewPets" options={{headerTitle: props => <LogoTitle {...props} />}}>

          {screenProps => (

            <ViewPets {...screenProps} updateAuthState={props.updateAuthState} />

          )}

        </AppStack.Screen>

      </AppStack.Navigator>

    );

  };

  const Initializing = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="tomato" />
      </View>
    );
  }



  useEffect(() => {
    checkAuthState();
  }, []);

  async function checkAuthState() {
    try {
      await Auth.currentAuthenticatedUser();
      console.log('✅ User is signed in');
      setUserLoggedIn('loggedIn');
    } catch (err) {
      console.log('❌ User is not signed in');
      setUserLoggedIn('loggedOut');
    }
  }

  function updateAuthState(isUserLoggedIn) {
    setUserLoggedIn(isUserLoggedIn);
  }




  return (
    <NavigationContainer>
      {isUserLoggedIn === 'initializing' && <Initializing />}
      {isUserLoggedIn === 'loggedIn' && (
        <AppNavigator updateAuthState={updateAuthState} />
      )}
      {isUserLoggedIn === 'loggedOut' && (
        <AuthenticationNavigator updateAuthState={updateAuthState} />
      )}
    </NavigationContainer>
  );
}




export default App;

